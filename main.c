#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int insertData();
void getData();
int deleteData(char hapus[]);

int main() {
    char choice;
    char nim_hapus[15];

    start:
    system("clear");
    printf(
        "===== PROGRAM MANAJEMEN DATA MAHASISWA =====\n"
        "============================================\n\n"
    );

    printf("Menu:\n1. Tambah Data\n2. Hapus Data\n3. Lihat Data\n4. Keluar\n");
    insert_choice:
    printf("Masukkan pilihan: ");
    scanf(" %c", &choice);

    switch(choice) {
        case '1':
            if(insertData()) printf("\nBerhasil menambahkan data!\n");
            else printf("\nGagal menambahkan data!");
            getchar();
            goto start;
            break;
        case '2':
            printf("\nMasukkan NIM yang dihapus: ");
            while(getchar() != '\n');
            fgets(nim_hapus, 15, stdin);
            nim_hapus[strcspn(nim_hapus, "\n")] = 0;
            if(deleteData(nim_hapus)) printf("\nBerhasil menghapus NIM %s.\n", nim_hapus);
            else printf("\nNIM yang dihapus tidak tersedia.\n");
            getchar();
            goto start;
            break;
        case '3':
            getData();
            getchar(); getchar();
            goto start;
            break;
        case '4':
            printf("\n");
            insert_exit:
            printf("Yakin keluar? (y/n): ");
            scanf(" %c", &choice);
            if(choice == 'y' || choice == 'Y') exit(0);
            else if(choice == 'n' || choice == 'N') goto start;
            else goto insert_exit;
            break;
        default:
            goto insert_choice;
            break;
    }

    return 0;
}

int insertData() {
    FILE *mhs_file;
    char nim[15], nama[100], phone[15], alamat[100];
    float ipk;

    printf("\nNIM\t: ");
    while(getchar() != '\n');
    fgets(nim, 15, stdin);
    nim[strcspn(nim, "\n")] = 0;

    printf("Nama\t: ");
    fgets(nama, 100, stdin);
    nama[strcspn(nama, "\n")] = 0;

    printf("IPK\t: ");
    scanf("%f", &ipk);

    printf("No. HP\t: ");
    while(getchar() != '\n');
    fgets(phone, 15, stdin);
    phone[strcspn(phone, "\n")] = 0;

    printf("Alamat\t: ");
    fgets(alamat, 100, stdin);
    alamat[strcspn(alamat, "\n")] = 0;

    mhs_file = fopen("data.csv", "a+");
    if(!mhs_file) {
        printf("Data mahasiswa tidak tersedia!\n");
        return 0;
    } else {
        fprintf(mhs_file, "%s;%s;%.2f;%s;%s;\n", nim, nama, ipk, phone, alamat);
        fclose(mhs_file);
    }

    return 1;
}

void getData() {
    char buffer[500];
    char *value;
    int count = 1;

    FILE *data_mhs;
    data_mhs = fopen("data.csv", "r");

    if(!data_mhs) {
        printf("Data mahasiswa tidak tersedia!\n");
    } else {
        while(fgets(buffer, 500, data_mhs)) {
            printf("\nMahasiswa ke-%d:\n", count);
            char *value = strtok(buffer, ";");
            printf("NIM\t: %s\n", value);
            value = strtok(NULL, ";");
            printf("Nama\t: %s\n", value);
            value = strtok(NULL, ";");
            printf("IPK\t: %s\n", value);
            value = strtok(NULL, ";");
            printf("No. HP\t: %s\n", value);
            value = strtok(NULL, ";");
            printf("Alamat\t: %s\n", value);
            count++;
        }
        fclose(data_mhs);
    }
}

int deleteData(char hapus[]) {
    char buffer[500];
    char *value;
    int status = 0;

    FILE *data_mhs, *data_baru;

    data_mhs = fopen("data.csv", "r");
    if(!data_mhs) {
        printf("Data mahasiswa tidak tersedia!\n");
    } else {
        data_baru = fopen("tmp.csv", "a+");
        while(fgets(buffer, 500, data_mhs)) {
            char *value = strtok(buffer, ";");
            if(strcmp(value, hapus) == 0) {
                status++;
                continue;
            }
            fprintf(data_baru, "%s;", value);
            value = strtok(NULL, ";");
            fprintf(data_baru, "%s;", value);
            value = strtok(NULL, ";");
            fprintf(data_baru, "%s;", value);
            value = strtok(NULL, ";");
            fprintf(data_baru, "%s;", value);
            value = strtok(NULL, ";");
            fprintf(data_baru, "%s;\n", value);
        }
        fclose(data_mhs);
        fclose(data_baru);
    }
    if(status != 0) {
        system("rm data.csv");
        system("mv tmp.csv data.csv");
        return 1;
    } else {
        system("rm tmp.csv");
        return 0;
    }
}
